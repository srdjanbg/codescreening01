package com.srdjan.codescreening01;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity 
{
	// screen fields
	private EditText editTxtHost;
	private EditText editTxtPort;
	private EditText editTxtGetChallenge;
	private EditText editTxtSolveChallenge;
	private TextView txtStatus;
	private TextView txtData;
	private TextView txtOrderData;
	private TextView txtSortedData;
	private Button buttonShowData;
	
	
	// declare input arrays as arraylists because of unknown size
	public ArrayList<String> input_arrayL  = new ArrayList<String>() ; // data array
	public ArrayList<String> input_arrayL1 = new ArrayList<String>() ; // order array
	
	private int TIMEOUT = 10000;
	
	// default values
	String hostPrefix = "http://";
	String defaulHost = "46.10.208.40";
	String defaulPort = "443";
	String defaultGetChallengeURI   = "/AndroidBackendServices/services/getChallenge";
	String defaultSolveChallengeURI = "/AndroidBackendServices/services/solveChallenge";
	
	// working values
	String newHost, newPort, newGetChallengeURI, newSolveChallengeURI;
	String txtHolderData = "";
	String txtHolderOrderData = "";
	String txtHolderSortedData = "";
	
	// result messages
	String result = "";
	Boolean isDataVisible = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		editTxtHost = (EditText)findViewById(R.id.editTextHost);
		editTxtPort = (EditText)findViewById(R.id.editTextPort);
		editTxtGetChallenge   = (EditText)findViewById(R.id.editTextGetChallenge);
		editTxtSolveChallenge = (EditText)findViewById(R.id.editTextSolveChallenge);
		txtStatus = (TextView)findViewById(R.id.textStatus);
		buttonShowData = (Button)findViewById(R.id.button2);
		
		txtData       = (TextView)findViewById(R.id.textData);
		txtOrderData  = (TextView)findViewById(R.id.textOrderData);
		txtSortedData = (TextView)findViewById(R.id.textSortedData);
		
		// set default values
		newHost = defaulHost;
		newPort = defaulPort;
		newGetChallengeURI = defaultGetChallengeURI;
		newSolveChallengeURI = defaultSolveChallengeURI;
		
		// put values to the screen
		editTxtHost.setText(newHost);
		editTxtPort.setText(newPort);
		editTxtGetChallenge.setText(newGetChallengeURI);
		editTxtSolveChallenge.setText(newSolveChallengeURI);
		txtStatus.setText(" ");
		txtData.setText(" ");
		txtOrderData.setText(" ");
		txtSortedData.setText(" ");
		
		txtData.setVisibility(View.GONE);
		txtOrderData.setVisibility(View.GONE);
		txtSortedData.setVisibility(View.GONE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	// set values back to defaults
	public void doDefaults(View view)
	{
		newHost = defaulHost;
		newPort = defaulPort;
		newGetChallengeURI = defaultGetChallengeURI;
		newSolveChallengeURI = defaultSolveChallengeURI;
		
		editTxtHost.setText(newHost);
		editTxtPort.setText(newPort);	
		editTxtGetChallenge.setText(newGetChallengeURI);
		editTxtSolveChallenge.setText(newSolveChallengeURI);		
	}
	
	// connect to service
	public void doConnect(View view)
	{		
		// get values from screen
		newHost = editTxtHost.getText().toString();
		newPort = editTxtPort.getText().toString();
		newGetChallengeURI   = editTxtGetChallenge.getText().toString();
		newSolveChallengeURI = editTxtSolveChallenge.getText().toString();				
		
		// debug data
		/*
		Log.d ("CS", "doConnect clicked");
		Log.d ("CS", "Host : " + newHost);
		Log.d ("CS", "Port : " + newPort);				
		Log.d ("CS", "Get  : " + newGetChallengeURI);
		Log.d ("CS", "Solve: " + newSolveChallengeURI);
		*/
		
		txtStatus.setText("");
		txtData.setText("");
		txtSortedData.setText("");
		txtOrderData.setText("");		
		
		txtHolderData = txtHolderOrderData = txtHolderSortedData = "";
		
		// get data from service
		getJSONData();        		
	}	
	
	public static String convertStreamToString(java.io.InputStream is) 
	{
	      java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	      return s.hasNext() ? s.next() : "";
	}
	
	
	// get and handle json data
	public void getJSONData()
	{
		// we need a new thread to get network connection
	      Thread thread = new Thread(new Runnable()
	      {
	          @Override
	          public void run() 
	          {
	        	  try
	        	  {
	        		  String url_final = hostPrefix + newHost + newGetChallengeURI;
	        		  
	        		  JSONHandler jsh = new JSONHandler();
	        		  jsh.setUrl( url_final );
	        		  
	        		  String data = jsh.getJSONData();
	        		  
	        		  parseJSONData(data);
	        		  doStuff();
	        	  }
	        	  catch (Exception e)
	        	  {
	        		  e.printStackTrace();
	        	  }	        	  
	          }
	       });
	        thread.start(); 			    
	}
	
	// parse arraylist data into arrays
	public void parseJSONData(String input)
	{		
		JSONObject jobj;

		input_arrayL.clear();
		input_arrayL1.clear();
		
		try 
		{
			jobj = new JSONObject(input);			
			JSONArray jarr = jobj.getJSONArray("dataValues");
			for (int i=0; i < jarr.length(); i++)
			{
				input_arrayL.add(jarr.get(i) + "");
			}
			
			jarr = jobj.getJSONArray("dataOrder");
			for (int i=0; i < jarr.length(); i++)
			{
				input_arrayL1.add(jarr.get(i) + "");
			}			
			
		} catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/*
	public void sendDataBack()
	{


	}
	*/
	public void doStuff()
	{
		String[] new_array     = input_arrayL.toArray( new String[ input_arrayL.size()] );     // unsorted array
		String[] new_array_old = input_arrayL.toArray( new String[ input_arrayL.size()] );     // unsorted copy
		String[] order_array   = input_arrayL1.toArray( new String[ input_arrayL1.size()] );   // sorting array

		/* sort */
		int temp;
		String temp2;
		
		Boolean flag = true;
		
		// sort orderData and use it to sort dataValues 
		while ( flag )
		{
			flag = false;
			{
				for (int j = 0; j<order_array.length-1; j++)
				{
					if ( Integer.parseInt(order_array[j]) > Integer.parseInt(order_array[j+1]))
					{
						temp = Integer.parseInt(order_array[j]);
						order_array[j] = order_array[j+1];
						order_array[j+1] = temp + "";
						flag = true;
						
						temp2 = new_array[j];
						new_array[j] = new_array[j+1];
						new_array[j+1] = temp2;
						flag = true;
						
					}
				}
			}
		}	
		
	    String url_final = hostPrefix + newHost + newSolveChallengeURI;
		JSONHandler jsh = new JSONHandler();
		jsh.setUrl( url_final );
		Log.d("CS", "URL: " + jsh.getUrl() );
		
    	// build JSON based on sorted array
        JSONObject json = new JSONObject();	        
        JSONArray ja = new JSONArray();
        
        for (int i=0; i<new_array.length; i++)
        {
        	ja.put(new_array[i]);
        }
        
        try 
        {
        	json.put( "solution", ja);
		} 
        catch (JSONException e) 
		{
        	// TODO Auto-generated catch block
        	e.printStackTrace();
		}	        		
		
        result = jsh.sendJSONData(json);
        Log.d("CS", result);
        
        // prepare data for screen output
        for (int i=0; i<new_array_old.length; i++)
        {
        	if ( i < new_array_old.length-1) txtHolderData += new_array_old[i] + ",";
        	else txtHolderData += new_array_old[i];
        }
        
        for (int i=0; i<new_array.length; i++)
        {
        	if ( i < new_array.length-1) txtHolderSortedData += new_array[i] + ",";
        	else txtHolderSortedData += new_array[i];
        }
        
        for (int i=0; i<order_array.length-1; i++)
        {
        	if ( i < order_array.length) txtHolderOrderData += order_array[i] + ",";
        	else txtHolderOrderData += order_array[i];
        }        
        
        // back to main thread
        runOnUiThread( new Runnable() 
        {			
			@Override
			public void run() 
			{
				txtStatus.setText(result);
				txtData.setText(txtHolderData);
				txtSortedData.setText(txtHolderSortedData);
				txtOrderData.setText( txtHolderOrderData );
				
				Toast.makeText(getBaseContext(), result, Toast.LENGTH_SHORT).show();
			}
		});

	}
	
	public void doShowData( View view)
	{
		if ( isDataVisible == true )
		{
			txtData.setVisibility(View.GONE);
			txtOrderData.setVisibility(View.GONE);
			txtSortedData.setVisibility(View.GONE);
			isDataVisible = false;
			buttonShowData.setText("SHOW DATA");
		}
		
		else
		{
			txtData.setVisibility(View.VISIBLE);
			txtOrderData.setVisibility(View.VISIBLE);
			txtSortedData.setVisibility(View.VISIBLE);
			isDataVisible = true;
			buttonShowData.setText("HIDE DATA");
		}		
	}

}
