package com.srdjan.codescreening01;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class JSONHandler 
{
	private String url;
	private int TIMEOUT = 10000;
	
	public void setUrl(String u)
	{
		url = u;
	}
	
	public String getUrl()
	{
		return url;
	}	
	
	public String getJSONData()
	{
		try
		{
			URL u = new URL( url );	        		  	
  			HttpURLConnection conn = (HttpURLConnection) u.openConnection();
  			conn.setReadTimeout(TIMEOUT);
  			conn.setConnectTimeout(TIMEOUT);
  			conn.setRequestMethod("GET");
  			conn.setDoInput(true); 
  			conn.connect();
  		
  			InputStream stream = conn.getInputStream();  		
  			String data = convertStreamToString(stream);  		
  		    stream.close();	
  		    
  			return data;
		}
		catch  (Exception e)
		{
			return "error";
		}
  		//Log.d("CS", data);		
	}
	
	public String sendJSONData( JSONObject json)
	{
		try
		{
        // connect to server and send data
        HttpParams httpParams = new BasicHttpParams();	        
        HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT);
        HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT);
        HttpClient client = new DefaultHttpClient(httpParams);

        //String url = url_final ;

        HttpPost httpPost = new HttpPost(url);
        List<NameValuePair> postParams = new ArrayList<NameValuePair>();
        postParams.add(new BasicNameValuePair("solution", json.toString()));

        UrlEncodedFormEntity ent = new UrlEncodedFormEntity(postParams);
        ent.setContentEncoding(HTTP.UTF_8);
        httpPost.setEntity(ent);	        
        	        	        	        
        // get response
        HttpResponse response = client.execute(httpPost);	        
        HttpEntity entity = response.getEntity();
         
        if (entity != null) 
        {
            InputStream instream = entity.getContent();
            String result = convertStreamToString(instream);
            
            // quick and dirty check for success
            if ( result.contains("SUCCESS") )	            
            {
            	return "SUCCESS";	            	    	 	            	               	
            }
        }
		}
		catch ( Exception e)
		{		
	        return "EXCEPTION";
		}
        
		return "ERROR";
	}

	private static String convertStreamToString(java.io.InputStream is) 
	{
	      java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	      return s.hasNext() ? s.next() : "";
	}
	
}
